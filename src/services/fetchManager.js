function fetchManager(url, options) {
    return fetch(url, {
        ...options,
        accept: "application/json"
    })
        .then(checkStatus)
        .then(parseJSON);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    const error = new Error(`HTTP Error ${response.statusText}`);
    error.status = response.statusText;
    error.response = response;
    throw error;
}

function parseJSON(response) {
    return response.json();
}

export const fetchMock = (data, delay = 1000) => new Promise(resolve => setTimeout(() => resolve(data), delay));

export default fetchManager;
