import { combineReducers } from 'redux';
import accounts from '../features/accounts/accountsReducer';
import exchange from '../features/exchange/exchangeReducer';
import user from '../features/user/userReducer';

export default combineReducers({
    accounts,
    exchange,
    user,
})