import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const CurrencyDropdown = ({ options, value, onChange }) => (
    <div className="text-center" style={{minWidth: 90, width:90}}>
        <Select
            isSearchable
            options={options.map(c => ({value: c, label: c}))}
            value={{ value, label: value }}
            onChange={selectedOption => onChange(selectedOption.value)}
        />
    </div>
);

CurrencyDropdown.propTypes = {
    options: PropTypes.array.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default CurrencyDropdown;