import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

const ErrorMessage = ({ message }) => (
    <Alert variant="danger" className="mt-1">
        {message}
    </Alert>
);

ErrorMessage.propTypes = {
    message: PropTypes.string,
};

export default ErrorMessage;