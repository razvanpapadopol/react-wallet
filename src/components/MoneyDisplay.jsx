import React from 'react';
import PropTypes from 'prop-types';
import { Col, Card } from 'react-bootstrap';

const MoneyDisplay = ({ amount, currency }) => (
    <Col xs={6} sm={4} md={3} lg={2} className="mb-2">
        <Card>
            <Card.Title>{Math.round(amount * 100) / 100}</Card.Title>
            <Card.Subtitle>{currency}</Card.Subtitle>
        </Card>
    </Col>
);

MoneyDisplay.propTypes = {
    amount: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
};

export default MoneyDisplay;