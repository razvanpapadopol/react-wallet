import React from 'react';
import PropTypes from 'prop-types';
import { Spinner } from 'react-bootstrap';

const LoadingSpinner = ({ label }) => (
    <div className="text-center">
        <h3>{label}</h3>
        <Spinner animation="border" variant='primary' />
    </div>
);

LoadingSpinner.propTypes = {
    label: PropTypes.string,
    center: PropTypes.bool,
};

export default LoadingSpinner;