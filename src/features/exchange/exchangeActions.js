import { 
    GET_EXCHANGE_RATES_PENDING, 
    GET_EXCHANGE_RATES_SUCCESS, 
    GET_EXCHANGE_RATES_FAILED,
} from "./exchangeReducer";
import fetchManager from "../../services/fetchManager";
import { deposit, withdraw } from "../accounts/accountsActions";

const EXCHANGE_RATES_URL = 'https://gist.githubusercontent.com/razvanpapadopol/e52071eb83a6a5d815abccf820948ca6/raw/4eb82de071a3b5db15012872764b3e0f88de06ae/exchangeRates.json'

export const getExchangeRates = () => dispatch => {    
    dispatch({ type: GET_EXCHANGE_RATES_PENDING });

    return fetchManager(EXCHANGE_RATES_URL)
        .then(response => dispatch({ type: GET_EXCHANGE_RATES_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: GET_EXCHANGE_RATES_FAILED }));
};

export const validateExchange = exchangeStatus => (dispatch, getState) => {
    const { 
        exchanging: { amount, currency }, 
        receiving: { 
            amount: newAmount, 
            currency: newCurrency 
        } 
    } = exchangeStatus;
    const sourceAccount = getState().accounts.accountList.find(account => account.currency === currency)
    
    if(!sourceAccount) {
        return Promise.resolve({ error: 'Currency not available!' });
    }
    
    if(sourceAccount.amount < amount) {
        return Promise.resolve({ error: 'There are not enough funds!' });
    }

    return Promise.all([
        dispatch(deposit(newAmount, newCurrency)),
        dispatch(withdraw(amount, currency)),
    ]);
};
