export const GET_EXCHANGE_RATES_PENDING = 'exchange/GET_EXCHANGE_RATES_PENDING';
export const GET_EXCHANGE_RATES_SUCCESS = 'exchange/GET_EXCHANGE_RATES_SUCCESS';
export const GET_EXCHANGE_RATES_FAILED = 'exchange/GET_EXCHANGE_RATES_FAILED';

export const initialState = {
    rates: {},
    isLoading: false,
};

const exchangeReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_EXCHANGE_RATES_PENDING:
            return {
                ...state,
                isLoading: true,
            };

        case GET_EXCHANGE_RATES_FAILED:
            return {
                ...state,
                isLoading: false,
            };

        case GET_EXCHANGE_RATES_SUCCESS:
            return {
                ...state,
                rates: action.payload.rates,
                isLoading: false,
            };

        default:
            return state
    }
};

export const selectExchangeRates = ({ exchange }) => exchange.rates;

export const selectCurrencies = ({ exchange }) => Object.keys(exchange.rates);

export const isExchangeDataLoading = ({ exchange }) => exchange.isLoading;

export default exchangeReducer;
