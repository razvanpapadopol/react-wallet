export const getReceivingInitialCurrency = (defaultCurrency, currencies) => 
    defaultCurrency !== currencies[0] ? currencies[0] : currencies[1];