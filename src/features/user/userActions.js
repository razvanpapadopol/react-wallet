import {
    GET_USER_PROFILE_PENDING,
    GET_USER_PROFILE_SUCCESS,
    GET_USER_PROFILE_FAILED,
    SET_USER_PROFILE_PENDING,
    SET_USER_PROFILE_SUCCESS,
    SET_USER_PROFILE_FAILED,
} from "./userReducer";
import fetchManager, { fetchMock } from "../../services/fetchManager";

const USER_PROFILE_URL = 'https://gist.githubusercontent.com/razvanpapadopol/9e1059fbd32efd5b13a35f24ebdadd80/raw/1c4bc0c43cde6c728249bea518c8e8d18dae49e8/user-profile';

export const getUserProfile = () => dispatch => {
    dispatch({ type: GET_USER_PROFILE_PENDING });

    return fetchManager(USER_PROFILE_URL)
        .then(response => dispatch({ type: GET_USER_PROFILE_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: GET_USER_PROFILE_FAILED }));
};

export const setUserProfile = userData => dispatch => {
    dispatch({ type: SET_USER_PROFILE_PENDING });

    return fetchMock(userData, 100)
        .then(response => dispatch({ type: SET_USER_PROFILE_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: SET_USER_PROFILE_FAILED }));
};
