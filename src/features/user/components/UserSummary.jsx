import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { selectUserProfile } from '../userReducer';
import CurrencySetting from './CurrencySetting';

const UserSummary = ({ user }) => {
    return (
        <Row className="mt-5">
            <Col xs={12} sm={5} md={5} lg={3} className="mb-1">
                <h3>{user.name}</h3>
                <p>{user.email}</p>
                <CurrencySetting />
            </Col>
        </Row>
    );
};

UserSummary.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
    }).isRequired,
};

const mapStateToProps = state => ({
    user: selectUserProfile(state),
});

export default connect(
    mapStateToProps,
    {}
)(UserSummary)
