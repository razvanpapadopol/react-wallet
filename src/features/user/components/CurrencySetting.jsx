import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CurrencyDropdown from '../../../components/CurrencyDropdown';
import { selectDefaultCurrency } from '../userReducer';
import { setUserProfile } from '../userActions';
import { selectCurrencies } from '../../exchange/exchangeReducer';

const CurrencySetting = ({ currencies, defaultCurrency, setUserProfile }) => {
    const handleChange = currency => setUserProfile({ currency });

    return (
        <React.Fragment>
            <h5>Set default currency</h5>
            <CurrencyDropdown
                options={currencies}
                value={defaultCurrency}
                onChange={handleChange}
            />
        </React.Fragment>
    );
};

CurrencySetting.propTypes = {
    defaultCurrency: PropTypes.string.isRequired,
    currencies: PropTypes.array.isRequired,
    setUserProfile: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    currencies: selectCurrencies(state),
    defaultCurrency: selectDefaultCurrency(state),
});

export default connect(
    mapStateToProps,
    { setUserProfile }
)(CurrencySetting)
