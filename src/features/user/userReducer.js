export const GET_USER_PROFILE_PENDING = 'user/GET_USER_PROFILE_PENDING';
export const GET_USER_PROFILE_SUCCESS = 'user/GET_USER_PROFILE_SUCCESS';
export const GET_USER_PROFILE_FAILED = 'user/GET_USER_PROFILE_FAILED';
export const SET_USER_PROFILE_PENDING = 'user/SET_USER_PROFILE_PENDING';
export const SET_USER_PROFILE_SUCCESS = 'user/SET_USER_PROFILE_SUCCESS';
export const SET_USER_PROFILE_FAILED = 'user/SET_USER_PROFILE_FAILED';

const DEFAULT_CURRENCY = 'USD';

export const initialState = {
    currency: DEFAULT_CURRENCY,
    name: '',
    email: '',
    isLoading: false,
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_USER_PROFILE_PENDING:
            return {
                ...state,
                isLoading: true,
            };

        case GET_USER_PROFILE_FAILED:
            return {
                ...state,
                isLoading: false,
            };

        case GET_USER_PROFILE_SUCCESS:
            return {
                ...state,
                ...action.payload,
                isLoading: false,
            };

        case SET_USER_PROFILE_SUCCESS:
            return {
                ...state,
                ...action.payload,
            };

        default:
            return state
    }
};

export const selectUserProfile = ({ user }) => ({ 
    name: user.name,
    email: user.email,
});

export const selectDefaultCurrency = ({ user }) => user.currency;

export const isUserDataLoading = ({ user }) => user.isLoading;

export default userReducer;
