import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import MoneyDisplay from '../../../components/MoneyDisplay';

const AccountList = ({ accounts }) => (
    <Row className="mt-5">
        <Col xs={12}>
            <h2>Accounts overview</h2>
        </Col>
        {accounts.map(account => (
            <MoneyDisplay 
                key={account.currency}
                amount={account.amount}
                currency={account.currency}
            />
        ))}
    </Row>
);

AccountList.propTypes = {
    accounts: PropTypes.array.isRequired,
};

export default AccountList;