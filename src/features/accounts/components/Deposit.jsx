import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Button, Spinner } from 'react-bootstrap';
import MoneyInput from '../../../components/MoneyInput';
import { deposit } from '../accountsActions';
import { selectCurrencies } from '../../exchange/exchangeReducer';
import { selectDefaultCurrency } from '../../user/userReducer';

export const Deposit = ({ currencies, defaultCurrency, deposit }) => {
    const initialDepositData = { amount: 0, currency: defaultCurrency };
    const isDepositLoading = false;
    
    const [ depositData, setDepositData ] = useState(initialDepositData);
    const [ isLoading, setDepositLoading ] = useState(isDepositLoading);

    const handleUpdate = newData => {
        setDepositData({
            ...depositData,
            ...newData,
        })
    }

    const handleSubmit = () => {
        setDepositLoading(true);
        deposit(depositData.amount, depositData.currency)
            .then(() => setDepositLoading(false));
    };

    useEffect(
        () => setDepositData({
            currency: defaultCurrency,
        }),
        [defaultCurrency]
    );

    return (
        <Row className="mt-5">
            <Col xs={12}>
                <h2>Add money</h2>
            </Col>
            <Col xs={12} sm={5} md={5} lg={3} className="mb-1">
                <MoneyInput 
                    currencies={currencies}
                    amount={depositData.amount}
                    selectedCurrency={depositData.currency}
                    onChange={handleUpdate}
                />
            </Col>
            <Col md={6} lg={3} className="mb-1">
                <Button 
                    disabled={!depositData.amount || depositData.amount <= 0} 
                    onClick={handleSubmit}
                >
                    Deposit 
                    {isLoading && <Spinner animation="border" size="sm" className="ml-1" />}
                </Button>
            </Col>
        </Row>
    );
};

Deposit.propTypes = {
    currencies: PropTypes.array.isRequired,
    defaultCurrency: PropTypes.string.isRequired,
    deposit: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    currencies: selectCurrencies(state),
    defaultCurrency: selectDefaultCurrency(state),
});

export default connect(
    mapStateToProps,
    { deposit }
)(Deposit)
