import { selectExchangeRates, isExchangeDataLoading } from "../exchange/exchangeReducer";
import { isUserDataLoading, selectDefaultCurrency } from "../user/userReducer";

export const GET_ACCOUNTS_PENDING = 'accounts/GET_ACCOUNTS_PENDING';
export const GET_ACCOUNTS_SUCCESS = 'accounts/GET_ACCOUNTS_SUCCESS';
export const GET_ACCOUNTS_FAILED = 'accounts/GET_ACCOUNTS_FAILED';
export const SET_DEPOSIT_SUCCESS = 'accounts/SET_DEPOSIT_SUCCESS';
export const SET_DEPOSIT_FAILED = 'accounts/SET_DEPOSIT_FAILED';
export const SET_WITHDRAWAL_SUCCESS = 'accounts/SET_WITHDRAWAL_SUCCESS';
export const SET_WITHDRAWAL_FAILED = 'accounts/SET_WITHDRAWAL_FAILED';

export const initialState = {
    accountList: [],
    error: null,
    isLoading: false,
};

const accountsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ACCOUNTS_PENDING:
            return {
                ...state,
                isLoading: true,
                error: null,
            };

        case GET_ACCOUNTS_FAILED:
            return {
                ...state,
                error: action.payload,
                isLoading: false,
            };

        case GET_ACCOUNTS_SUCCESS:
            return {
                ...state,
                accountList: action.payload,
                isLoading: false,
            };

        case SET_DEPOSIT_SUCCESS:
            if(state.accountList.every(account => account.currency !== action.payload.currency)) {
                return {
                    ...state,
                    accountList: [...state.accountList, action.payload],
                };
            }

            return {
                ...state,
                accountList: state.accountList.map(account => {
                    if(account.currency === action.payload.currency) {
                        return {
                            ...account,
                            amount: account.amount + action.payload.amount,
                        };
                    }

                    return account;
                })
            };

        case SET_WITHDRAWAL_SUCCESS: 
            return {
                ...state,
                accountList: state.accountList.map(account => {
                    if(account.currency === action.payload.currency) {
                        return {
                            ...account,
                            amount: account.amount - action.payload.amount,
                        };
                    }
                        
                    return account;
                }),
            };

        default:
            return state
    }
};

export const isDataLoading = state => state.accounts.isLoading || isExchangeDataLoading(state) || isUserDataLoading(state);

export const selectAccounts = ({ accounts }) => accounts.accountList;

export const selectAccountsError = ({ accounts }) => accounts.error;

export const selectTotalBalance = state => {
    const accounts = state.accounts.accountList;
    const exchangeRates = selectExchangeRates(state);
    const defaultCurrency = selectDefaultCurrency(state);

    return {
        amount: accounts.reduce((total, account) => {
            return (account.amount / exchangeRates[account.currency] + total) * exchangeRates[defaultCurrency];
        }, 0),
        currency: defaultCurrency,
    }
};

export default accountsReducer
