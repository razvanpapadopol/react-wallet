import accountsReducer, {
    GET_ACCOUNTS_PENDING,
    GET_ACCOUNTS_SUCCESS,
    GET_ACCOUNTS_FAILED,
    SET_DEPOSIT_SUCCESS,
    SET_WITHDRAWAL_SUCCESS,
    initialState,
} from './accountsReducer';

describe('accountsReducer', () => {
    it('should return the initial state', () => {
        expect(accountsReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_ACCOUNTS_PENDING', () => {
        const state = initialState;
        const expectedState = {
            ...state,
            isLoading: true,
            error: null,
        };

        expect(
            accountsReducer(state, {
                type: GET_ACCOUNTS_PENDING,
            })
        ).toEqual(expectedState);
    });

    it('should handle GET_ACCOUNTS_FAILED', () => {
        const state = {
            ...initialState,
            isLoading: true,
            error: null,
        };
        const mockError = 'sample error';
        const expectedState = {
            ...state,
            isLoading: false,
            error: mockError,
        };

        expect(
            accountsReducer(state, {
                type: GET_ACCOUNTS_FAILED,
                payload: mockError,
            })
        ).toEqual(expectedState);
    });

    it('should handle GET_ACCOUNTS_SUCCESS', () => {
        const state = {
            ...initialState,
            isLoading: true,
        };
        const accountList = [{ currency: 'USD', amount: 100 }];

        expect(
            accountsReducer(state, {
                type: GET_ACCOUNTS_SUCCESS,
                payload: accountList,
            })
        ).toEqual({
            ...state,
            isLoading: false,
            accountList: accountList,
        })
    });

    it('should handle SET_DEPOSIT_SUCCESS with a new currency', () => {
        const accountList = [{ currency: 'USD', amount: 100 }];
        const depositData = { currency: 'EUR', amount: 100 }
        const state = {
            ...initialState,
            accountList,
        };

        expect(
            accountsReducer(state, {
                type: SET_DEPOSIT_SUCCESS,
                payload: depositData,
            })
        ).toEqual({
            ...state,
            accountList: [...accountList, depositData],
        })
    });

    it('should handle SET_DEPOSIT_SUCCESS with an existing currency', () => {
        const accountList = [{ currency: 'USD', amount: 100 }];
        const depositData = { currency: 'USD', amount: 200 }
        const state = {
            ...initialState,
            accountList,
        };

        expect(
            accountsReducer(state, {
                type: SET_DEPOSIT_SUCCESS,
                payload: depositData,
            })
        ).toEqual({
            ...state,
            accountList: [{ currency: 'USD', amount: 300 }],
        })
    });

    it('should handle SET_WITHDRAWAL_SUCCESS', () => {
        const accountList = [{ currency: 'USD', amount: 100 }];
        const withdrawalData = { currency: 'USD', amount: 70 }
        const state = {
            ...initialState,
            accountList,
        };

        expect(
            accountsReducer(state, {
                type: SET_WITHDRAWAL_SUCCESS,
                payload: withdrawalData,
            })
        ).toEqual({
            ...state,
            accountList: [{ currency: 'USD', amount: 30 }],
        })
    });
});
