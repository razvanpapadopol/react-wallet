import fetchManager, { fetchMock } from '../../services/fetchManager';
import {
    GET_ACCOUNTS_PENDING,
    GET_ACCOUNTS_SUCCESS,
    GET_ACCOUNTS_FAILED,
    SET_DEPOSIT_SUCCESS,
    SET_DEPOSIT_FAILED,
    SET_WITHDRAWAL_SUCCESS,
    SET_WITHDRAWAL_FAILED,
} from './accountsReducer';

export const GET_ACCOUNTS_ERROR_MESSAGE = 'Accounts failed to load. Please try again.';
const ACCOUNTS_URL = 'https://gist.githubusercontent.com/razvanpapadopol/bac00815fb293b29cbdd869e7052749c/raw/f22f3fc5d69f5ac8f873ef2dac185f8f23e4e635/accounts.json';

export const getAccounts = () => dispatch => {
    dispatch({ type: GET_ACCOUNTS_PENDING });

    return fetchManager(ACCOUNTS_URL)
        .then(response => dispatch({ type: GET_ACCOUNTS_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: GET_ACCOUNTS_FAILED, payload: GET_ACCOUNTS_ERROR_MESSAGE }));
};

export const deposit = (amount, currency) => dispatch => {
    return fetchMock({ amount, currency })
        .then(response => dispatch({ type: SET_DEPOSIT_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: SET_DEPOSIT_FAILED }));
}

export const withdraw = (amount, currency) => dispatch => {
    return fetchMock({ amount, currency })
        .then(response => dispatch({ type: SET_WITHDRAWAL_SUCCESS, payload: response }))
        .catch(() => dispatch({ type: SET_WITHDRAWAL_FAILED }));
}
