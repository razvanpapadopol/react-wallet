import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LoadingSpinner from './components/LoadingSpinner';
import { getAccounts } from './features/accounts/accountsActions';
import { selectAccounts, selectAccountsError, isDataLoading } from './features/accounts/accountsReducer';
import { getUserProfile } from './features/user/userActions';
import { getExchangeRates } from './features/exchange/exchangeActions';
import UserSummary from './features/user/components/UserSummary';
import CurrencyExchange from './features/exchange/components/CurrencyExchange';
import AccountList from './features/accounts/components/AccountList';
import TotalBalance from './features/accounts/components/TotalBalance';
import Deposit from './features/accounts/components/Deposit';
import { Alert } from 'react-bootstrap';

const DashboardScreen = props => {
    useEffect(() => {
        const { getUserProfile, getAccounts, getExchangeRates } = props;
        getAccounts();
        getExchangeRates();
        getUserProfile();
    }, [])

    const { isLoading, accounts, error } = props;

    if (error) {
        return <Alert variant="danger">{error}</Alert>
    }

    if (!accounts.length || isLoading) {
        return <LoadingSpinner center label="Please wait. Loading data..." />
    }

    return (
        <React.Fragment>
            <aside>
                <UserSummary />
                <Deposit />
            </aside>
            <section>
                {!accounts.length && !isLoading && (
                    <Alert variant="secondary" className="mt-5">
                        No active accounts. Please make a deposit first.
                    </Alert>
                )}
                {!!accounts.length && (
                    <React.Fragment>
                        <CurrencyExchange />
                        <AccountList accounts={accounts} />
                        <TotalBalance />
                    </React.Fragment>
                )}
            </section>
        </React.Fragment>
    )
}

DashboardScreen.propTypes = {
    accounts: PropTypes.array.isRequired,
    getAccounts: PropTypes.func.isRequired,
    error: PropTypes.string,
};

const mapStateToProps = state => ({
    accounts: selectAccounts(state),
    isLoading: isDataLoading(state),
    error: selectAccountsError(state),
});

export default connect(
    mapStateToProps,
    { getAccounts, getExchangeRates, getUserProfile }
)(DashboardScreen);
